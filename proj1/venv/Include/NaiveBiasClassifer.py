from Service import *
from nltk.classify import NaiveBayesClassifier
from nltk.classify.util import accuracy

SPLIT_ARRAY = [0.70, 0.75, 0.80, 0.85, 0.90]
FEATURE_DETECTOR_ARRAY = [
                          main_bag_of_words,
                          main_bag_of_words_without_badwords,
                          main_bag_of_bigrams_words,
                          main_bag_of_bigrams_words_without_badwords]

reader = CategorizedPlaintextCorpusReader(CORPUS_ROOT, r'.*_\w+_.*\.txt', cat_pattern=r'.*_(\w+)_.*\.txt')

print('categories')
print(reader.categories())
print('good')
print(reader.fileids(categories=['good']))
print('bad')
print(reader.fileids(categories=['bad']))
print('----------')

numberEx = 1
result = []
for i in range(2):
    for feature_director in FEATURE_DETECTOR_ARRAY:
        for stlit in SPLIT_ARRAY:
            use_hypernymys = i==1
            str_num = 'experiment number: ' + str(numberEx)
            str_split = 'split: ' + str(stlit)
            str_feature_director = 'feature director: ' + str(feature_director)
            str_use_hypernymys = 'use hypernymys: ' + str(use_hypernymys)
            print(str_num)
            print(str_split)
            print(str_feature_director)
            print(str_use_hypernymys)
            numberEx += 1


            print('----------')

            lfeats = label_feats_from_corpus(reader, feature_director, use_hypernymys)
            train_feats, test_feats = split_label_feats(lfeats, stlit)

            print(lfeats)
            print('----------')

            nb_classifer = NaiveBayesClassifier.train(train_feats)
            acc_val = accuracy(nb_classifer, test_feats)
            str_acc_val = 'acc_val: ' + str(acc_val)
            print(str_acc_val)

            info_lst = nb_classifer.show_most_informative_features(7)
            print(info_lst)
            print('----------')

            result.append('{:<30s} {:<20s} {:<90s} {:<20s} {:<20s}'.format(str_num,str_split,str_feature_director,str_use_hypernymys, str_acc_val))
for x in result: print(x)

