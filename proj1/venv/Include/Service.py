import collections
import re
import string
import pymorphy2
import numpy
from nltk import *
# 2nd
from nltk.collocations import BigramCollocationFinder
from nltk.corpus import PlaintextCorpusReader
from nltk.corpus import stopwords
from nltk.corpus.reader import CategorizedPlaintextCorpusReader
from nltk.metrics import BigramAssocMeasures
from wiki_ru_wordnet import WikiWordnet

CORPUS_ROOT = '../Resources/corpus'
MAX_LEN = 15
STOP_WORD_LIST = set(stopwords.words('russian'))
PRINT_COUNT = 10
wikiwordnet = WikiWordnet()
morph = pymorphy2.MorphAnalyzer()


# 1st semester


def prepareFunc(words):
    return [word.lower() for word in words if re.search('\w\d*', word)]


def lexicalDiversityFunc(corpus):
    wordList = prepareFunc(corpus.words())
    lexicalDiversity = len(set(wordList)) / len(corpus.words())
    print('lexicalDiversity  = \t\t\t\t', lexicalDiversity)

    wordListsWithoutStops = [word for word in wordList if word not in STOP_WORD_LIST]
    lexicalDiversityWithoutStops = len(set(wordListsWithoutStops)) / len(wordListsWithoutStops)
    print('lexicalDiversity without stops = \t', lexicalDiversityWithoutStops)
    print('----------')
    return


def mostCommonFunc(corpus):
    wordList = prepareFunc(corpus.words())
    fdist = FreqDist(wordList)
    mostCommon = fdist.most_common(PRINT_COUNT)
    print('mostCommon  = \t\t\t\t', mostCommon)

    fdist.plot(50, cumulative=True)

    fdist = FreqDist(word for word in wordList if word not in STOP_WORD_LIST)
    mostCommon = fdist.most_common(PRINT_COUNT)
    print('mostCommon without stops = \t', mostCommon)

    fdist.plot(50, cumulative=True)

    print('----------')
    return


def biggestWordsFunc(corpus, maxlen):
    wordList = prepareFunc(corpus.words())
    biggestWords = [word for word in wordList if len(word) > maxlen]
    print('Words, bigger ', maxlen, ' = \t\t\t\t', biggestWords)

    biggestWordsWithoutStops = [word for word in biggestWords if word not in STOP_WORD_LIST]
    print('Words, bigger ', maxlen, 'without stops = \t', biggestWords)

    print('----------')
    return


def mostCommonBigramsFunc(corpus):
    wordList = prepareFunc(corpus.words())
    bgr = bigrams(wordList)
    fdist = FreqDist(bgr)
    mostCommon = fdist.most_common(PRINT_COUNT)
    print('mostCommonBigrams  = \t\t\t\t', mostCommon)

    bgr = bigrams(word for word in wordList if word not in STOP_WORD_LIST)
    fdist = FreqDist(bgr)
    mostCommon = fdist.most_common(PRINT_COUNT)
    print('mostCommonBigrams without stops = \t', mostCommon)

    print('----------')
    return


#  2nd semester

# help func
def prepare_words_without_badwords_from_file(words, file='russian'):
    bad_words = stopwords.words(file)
    return prapare_words_without_badwords(words, bad_words)


def prapare_words_without_badwords(words, bad_words):
    return set(prepareFunc(words)) - set(bad_words)


def bag_of_words(words, useHypernymys=False):
    if (useHypernymys):
        new_dict = {}
        for word in words:
            if (isinstance(word, str)):
                new_dict.update({get_hypernyms(word): True})
            else:
                list = []
                for w in word:
                    list.append(get_hypernyms(w))
                new_dict.update({tuple(list): True})
        return new_dict
    else:
        return dict([(word, True) for word in words])


def bag_of_bigrams_words(words, useHypernymys, score_fn=BigramAssocMeasures.chi_sq, n=200):
    bigram_finder = BigramCollocationFinder.from_words(prepareFunc(words))
    bigrams = bigram_finder.nbest(score_fn, n)
    return bag_of_words(bigrams, useHypernymys)


def get_lemma_word(word):
    lemmas = []
    p = morph.parse(word)[0]
    lemmas.append(p.normal_form)
    return lemmas[0]


def get_hypernyms(word):
    result = word
    lemma = get_lemma_word(word)
    synsets = wikiwordnet.get_synsets(lemma)
    if (len(synsets) != 0):
        synset = synsets[0]
        hypernyms = list(wikiwordnet.get_hypernyms(synset))
        if (len(hypernyms) != 0):
            list_synonym = list(hypernyms[0].get_words())
            result = list_synonym[0].lemma()
    return result


# main func
def main_bag_of_words(words, useHypernymys):
    words = prepareFunc(words)
    return bag_of_words(words, useHypernymys)


def main_bag_of_words_without_badwords(words, useHypernymys):
    words = prepare_words_without_badwords_from_file(words)
    return bag_of_words(words, useHypernymys)


def main_bag_of_bigrams_words(words, useHypernymys, score_fn=BigramAssocMeasures.chi_sq, n=200):
    words = prepareFunc(words)
    return bag_of_bigrams_words(words, useHypernymys, score_fn, n)


def main_bag_of_bigrams_words_without_badwords(words, useHypernymys, score_fn=BigramAssocMeasures.chi_sq, n=200):
    words = prepare_words_without_badwords_from_file(words)
    return bag_of_bigrams_words(words, useHypernymys, score_fn, n)


def label_feats_from_corpus(corp, feature_detector, useHypernymys=False):
    label_feats = collections.defaultdict(list)
    for label in corp.categories():
        for fileid in corp.fileids(categories=[label]):
            feats = feature_detector(corp.words(fileids=[fileid]), useHypernymys)
            label_feats[label].append(feats)
    return label_feats


def split_label_feats(lfeats, split):
    train_feats = []
    test_feats = []
    for label, feats in lfeats.items():
        cutoff = int(len(feats) * split)
        train_feats.extend([feat, label] for feat in feats[:cutoff])
        test_feats.extend([feat, label] for feat in feats[cutoff:])
    return train_feats, test_feats
