from Service import *

goodWordCorpus = PlaintextCorpusReader(CORPUS_ROOT, '.*good.*')
badWordCorpus = PlaintextCorpusReader(CORPUS_ROOT, '.*bad.*')

print('good corpus')
print(goodWordCorpus.fileids())
print('bad corpus')
print(badWordCorpus.fileids())
print('----------')

vkWordCorpus = PlaintextCorpusReader(CORPUS_ROOT, '.*vk.*')
okWordCorpus = PlaintextCorpusReader(CORPUS_ROOT, '.*ok.*')
twitterWordCorpus = PlaintextCorpusReader(CORPUS_ROOT, '.*twitter.*')
telegramWordCorpus = PlaintextCorpusReader(CORPUS_ROOT, '.*telegram.*')
fbWordCorpus = PlaintextCorpusReader(CORPUS_ROOT, '.*fb.*')

print('vk corpus')
print(vkWordCorpus.fileids())
print('ok corpus')
print(okWordCorpus.fileids())
print('twitter corpus')
print(twitterWordCorpus.fileids())
print('telegram corpus')
print(telegramWordCorpus.fileids())
print('fb corpus')
print(fbWordCorpus.fileids())
print('----------')

# 1
print('№1')
print('goodCorpus')
lexicalDiversityFunc(goodWordCorpus)

print('badCorpus')
lexicalDiversityFunc(badWordCorpus)

# 2, 3
print('№2')
print('goodCorpus')
mostCommonFunc(goodWordCorpus)

print('badCorpus')
mostCommonFunc(badWordCorpus)

# 4
print('№4')
print('goodCorpus')
biggestWordsFunc(goodWordCorpus, MAX_LEN)

print('badCorpus')
biggestWordsFunc(badWordCorpus, MAX_LEN)

# 5
print('№5')
print('goodCorpus')
mostCommonBigramsFunc(goodWordCorpus)

print('badCorpus')
mostCommonBigramsFunc(badWordCorpus)
